package uz.pdp.upload.controller;

import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.upload.entity.Attachment;
import uz.pdp.upload.repository.AttachmentRepository;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/attachment")
@RequiredArgsConstructor
public class AttachmentController {

    private final AttachmentRepository attachmentRepository;

    @PostMapping("/upload")
    public HttpEntity<?> upload(MultipartHttpServletRequest request) {

        MultipartFile multipartFile = request.getFile("ketmon");

        String contentType = multipartFile.getContentType();
        long size = multipartFile.getSize();
        String name = multipartFile.getOriginalFilename();

        Attachment attachment = Attachment.builder()
                .contentType(contentType)
                .name(name)
                .size(size)
                .path(filePath(multipartFile, name))
                .build();
        attachmentRepository.save(attachment);
        return ResponseEntity.ok(attachment);
    }


    @PostMapping("/upload-by-size")
    public HttpEntity<?> uploadBySize(MultipartHttpServletRequest request) throws IOException {

        MultipartFile multipartFile = request.getFile("ketmon");

        if ((Objects.equals(multipartFile.getContentType(), "image/jpeg")
                || multipartFile.getContentType().equals("image/png")
                || multipartFile.getContentType().equals("image/jpg"))
                && multipartFile.getSize() > 2048000) {
            throw new RuntimeException("Rasm hajmi 2MB dan oshmasin");
        }

        if (request.getParameter("type").equals("avatar")) {
            if (multipartFile.getSize() > 512000)
                throw new RuntimeException("Avatar uchun 512KB dan oshmasin");


            BufferedImage image = ImageIO.read(multipartFile.getInputStream());
            int height = image.getHeight();
            int width = image.getWidth();
            if (height != 600 || width != 600) {
                throw new RuntimeException("OKa rasm 600x600 bo'lsin");
            }
        }


        String contentType = multipartFile.getContentType();
        long size = multipartFile.getSize();
        String name = multipartFile.getOriginalFilename();

        Attachment attachment = Attachment.builder()
                .contentType(contentType)
                .name(name)
                .size(size)
                .path(filePath(multipartFile, name))
                .build();
        attachmentRepository.save(attachment);
        return ResponseEntity.ok(attachment);

    }

    @GetMapping("/download/{id}")
    public HttpEntity<?> download(@PathVariable Integer id) throws IOException {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new RuntimeException("Attachment not found: " + id));
        byte[] bytes = Files.readAllBytes(Path.of(attachment.getPath()));

        return ResponseEntity.status(HttpStatus.OK)
                .header("Content-Disposition", "attachment; filename=" + attachment.getName())
                .header("Content-Type", attachment.getContentType())
                .body(bytes);
    }


    @SneakyThrows
    private String filePath(MultipartFile multipartFile, String name) {
        String rootPath = "D:\\g25";
        String uuid = UUID.randomUUID().toString();
        String extension = name.substring(name.lastIndexOf("."));
        String fileName = uuid + extension;
        Path path = Path.of(rootPath, fileName);
        Files.copy(multipartFile.getInputStream(), path);
        return rootPath + "/" + fileName;
    }
}
