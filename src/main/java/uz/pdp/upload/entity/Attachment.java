package uz.pdp.upload.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    private long size;

    @Column(nullable = false)
    private String contentType;

    @Column(nullable = false, unique = true)
    private String path; //D:/g25/uuid.jpg
}
